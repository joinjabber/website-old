+++
title = "Gemeenschap"
+++

Wij zijn een nieuw collectief dat de gebruikerservaring in het Jabber/XMPP-ecosysteem wil helpen verbeteren. Je kunt meer lezen over [onze doelen](@/collective/about/goals/index.md), of lees onze laatste [notulen van vergaderingen](@/collective/_index.md). We hebben geen enkele vorm van formeel lidmaatschap, dus als je het gevoel hebt dat we enkele gemeenschappelijke interesses hebben, is je deelname welkom!

Wil je met ons in contact komen? Bijdragen aan het project? We hebben verschillende chatrooms waar je lid van kunt worden. Op dit moment zijn al deze chatrooms in het Engels, maar we verwelkomen initiatieven om nieuwe chatrooms voor andere talen te creëren:

- [chat@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=chat%40joinjabber.org) ([xmpp](xmpp:chat@joinjabber.org?join)): een algemene chatroom voor het collectief, om vragen te stellen en betrokken te raken
- [support@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=support%40joinjabber.org) ([xmpp](xmpp:support@joinjabber.org?join)): algemene ondersteuning voor Jabber/XMPP-gebruikers
- [abuse@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=abuse%40joinjabber.org) ([xmpp](xmpp:abuse@joinjabber.org?join)): misbruik en moderatie in de Jabber/XMPP-federatie
- [privacy@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=privacy%40joinjabber.org) ([xmpp](xmpp:privacy@joinjabber.org?join)): discussies over privacy en veiligheid, voor gebruikers en serverbeheerders
- [translations@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=translations%40joinjabber.org) ([xmpp](xmpp:translations@joinjabber.org?join)): vertalingen voor onze bronnen, voor wereldwijde toegankelijkheid
- [sysadmin@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=sysadmin%40joinjabber.org) ([xmpp](xmpp:sysadmin@joinjabber.org?join)): systeembeheer en reproduceerbare infrastructuur voor onze collectieve
- [website@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=website%40joinjabber.org) ([xmpp](xmpp:website@joinjabber.org?join)): hoe we onze website kunnen verbeteren, en hoe jij kunt helpen
