+++
title = "Communauté"
+++

Nous sommes un nouveau collective qui souhaite aider à améliorer l'expérience utilisateurice dans l'écosystème Jabber/XMPP. Tu peux découvrir [nos objectifs](@/collective/about/goals/index.fr.md), ou lire [nos derniers comptes-rendus](@/collective/_index.fr.md). Nous n'avons pas d'adhésions formelles, donc si tu sens que nous avons des intérêts en commun, ta participation est bienvenue!

Tu veux rentrer en contact? Participer au projet? Nous avons différentes chatrooms (salons) que tu peux rejoindre. Pour le moment, toutes ces chatrooms sont en Anglais, mais nous accueillons les initiatives pour en créer pour d'autres langues:

- [chat@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=chat%40joinjabber.org) ([xmpp](xmpp:chat@joinjabber.org?join)): discussions générales sur le collectif, et comment participer
- [support@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=support%40joinjabber.org) ([xmpp](xmpp:support@joinjabber.org?join)): support pour les utilisateurices de Jabber/XMPP
- [abuse@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=abuse%40joinjabber.org) ([xmpp](xmpp:abuse@joinjabber.org?join)): abus et modération dans la fédération Jabber/XMPP
- [privacy@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=privacy%40joinjabber.org) ([xmpp](xmpp:privacy@joinjabber.org?join)): discussions sur la vie privée et la sécurité, pour utilisateurices et admins de serveurs
- [translations@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=translations%40joinjabber.org) ([xmpp](xmpp:translations@joinjabber.org?join)): traductions de nos ressources, pour une accessibilité mondiale
- [sysadmin@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=sysadmin%40joinjabber.org) ([xmpp](xmpp:sysadmin@joinjabber.org?join)): administration système et infrastructure reproductible pour notre collectif
- [website@joinjabber.org (web chat)](https://chat.joinjabber.org/#/guest?join=website%40joinjabber.org) ([xmpp](xmpp:website@joinjabber.org?join)): comment améliorer notre site web, et comment tu peux aider
